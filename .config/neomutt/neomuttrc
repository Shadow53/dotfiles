set my_default_account = mnbryant_personal

# PGP options
#set crypt_replysign
#set crypt_replyencrypt
#set crypt_replysignencrypted

# General Global Settings
set wait_key                    = no
set mbox_type                   = Maildir
set timeout                     = 3
set mail_check                  = 0
set delete
set abort_noattach
set help
set quit                        = ask-yes
set sort                        = date-received
set thorough_search
set mail_check_stats
set folder                      = "$HOME/.mail"

# Compose view settings
#set envelope_from
set edit_headers
set fast_reply
set fcc_attach
set forward_format              = "Fwd: %s"
set forward_decode
set attribution                 = "On %d, %n wrote:"
set reply_to
set reverse_name
set include
set forward_quote
set editor                      = "nvim"
set text_flowed

# status bar, date format, finding stuff etc.
set status_chars = " *%A"
set status_format = "[ Folder: %f ] [%r%m messages%?n? (%n new)?%?d? (%d to delete)?%?t? (%t tagged)? ]%>─%?p?( %p postponed )?"
set date_format = "%d.%m.%Y %H:%M"
set index_format = "[%Z] %?X?A&-? %D  %-20.20F  %s"
set sort = threads
set sort_aux = reverse-last-date-received
set uncollapse_jump
set sort_re
set reply_regexp = "^(([Rr][Ee]?(\[[0-9]+\])?: *)?(\[[^]]+\] *)?)*"
set quote_regexp = "^( {0,4}[>|:#%]| {0,4}[a-z0-9]+[>|]+)+"
set send_charset = "utf-8:iso-8859-1:us-ascii"
set charset = "utf-8"

# Pager View Options
set pager_index_lines = 10
set pager_context = 3
set pager_stop
set menu_scroll
set tilde
unset markers

# Email headers / attachments
ignore                          *
unignore                        from: to: cc: bcc: date: subject:
unhdr_order                     *
hdr_order                       from: to: cc: bcc: date: subject:
alternative_order               text/plain text/enriched text/html
auto_view                       text/html

# Get addresses from addressbook with khard and then mu index
set query_command               = "( khard email --parsable '%s' | sed -n '1!p'; mu cfind --format=mutt-ab '%s' )"

# Sidebar settings
set sidebar_visible
set sidebar_short_path
set sidebar_folder_indent
set sidebar_width = 30
set sidebar_divider_char = ' | '
set sidebar_indent_string = '  ''
set sidebar_format = "%D %?N?(%N)?%* %S"
#set sidebar_format = "%B %* [%?N?%N / ?%S]"

# Mailboxes to show in the sidebar.

set my_bnadmin                 = admin@bluestnight.com
set my_bnshadow53              = shadow53@bluestnight.com
set my_mnbadmin                = admin@mnbryant.com
set my_mnbaccounts             = myaccounts@mnbryant.com
set my_mnbpersonal             = michael@mnbryant.com
set my_sh53admin               = admin@shadow53.com
set my_sh53personal            = michael@shadow53.com
set my_sh53shadow53            = shadow53@shadow53.com
set my_google                  = mbryant.shadow53@gmail.com
set my_outlook                 = linuxer53@outlook.com
set my_outlook_alt             = laughinginthenight@outlook.com
set my_outlook_sh53            = mbryant.shadow53@outlook.com
set my_spu                     = bryantm1@spu.edu
set my_vivaldi                 = laughinginthenight@vivaldi.net

 named-mailboxes \
     "== BluestNight Admin"         +$my_bnadmin/INBOX \
     "   Archive"                   +$my_bnadmin/archive \
     "   Drafts"                    +$my_bnadmin/drafts \
     "   Junk"                      +$my_bnadmin/junk \
     "   Sent"                      +$my_bnadmin/sent \
     "   Trash"                     +$my_bnadmin/trash \
     "== BluestNight Shadow53"      +$my_bnshadow53/INBOX \
     "   Archive"                   +$my_bnshadow53/archive \
     "   Drafts"                    +$my_bnshadow53/drafts \
     "   Junk"                      +$my_bnshadow53/junk \
     "   Sent"                      +$my_bnshadow53/sent \
     "   Trash"                     +$my_bnshadow53/trash \
     "== Google"                    +$my_google/INBOX \
     "   Drafts"                    +$my_google/drafts \
     "   Junk"                      +$my_google/junk \
     "   Sent"                      +$my_google/sent \
     "   Trash"                     +$my_google/trash \
     "== MNBryant Admin"            +$my_mnbadmin/INBOX \
     "   Archive"                   +$my_mnbadmin/archive \
     "   Drafts"                    +$my_mnbadmin/drafts \
     "   Junk"                      +$my_mnbadmin/junk \
     "   Sent"                      +$my_mnbadmin/sent \
     "   Trash"                     +$my_mnbadmin/trash \
     "== MNBryant Accounts"         +$my_mnbaccounts/INBOX \
     "   Archive"                   +$my_mnbaccounts/archive \
     "   Drafts"                    +$my_mnbaccounts/drafts \
     "   Junk"                      +$my_mnbaccounts/junk \
     "   Sent"                      +$my_mnbaccounts/sent \
     "   Trash"                     +$my_mnbaccounts/trash \
     "== MNBryant Personal"         +$my_mnbpersonal/INBOX \
     "   Archive"                   +$my_mnbpersonal/archive \
     "   Drafts"                    +$my_mnbpersonal/drafts \
     "   Junk"                      +$my_mnbpersonal/junk \
     "   Sent"                      +$my_mnbpersonal/sent \
     "   Trash"                     +$my_mnbpersonal/trash \
     "== Outlook Linuxer53"         +$my_outlook/INBOX \
     "   Archive"                   +$my_outlook/archive \
     "   Drafts"                    +$my_outlook/drafts \
     "   Junk"                      +$my_outlook/junk \
     "   Sent"                      +$my_outlook/sent \
     "   Trash"                     +$my_outlook/trash \
     "== Outlook Laughing"          +$my_outlook_alt/INBOX \
     "   Archive"                   +$my_outlook_alt/archive \
     "   Drafts"                    +$my_outlook_alt/drafts \
     "   Junk"                      +$my_outlook_alt/junk \
     "   Sent"                      +$my_outlook_alt/sent \
     "   Trash"                     +$my_outlook_alt/trash \
     "== Outlook Shadow53"          +$my_outlook_sh53/INBOX \
     "   Archive"                   +$my_outlook_sh53/archive \
     "   Drafts"                    +$my_outlook_sh53/drafts \
     "   Junk"                      +$my_outlook_sh53/junk \
     "   Sent"                      +$my_outlook_sh53/sent \
     "   Trash"                     +$my_outlook_sh53/trash \
     "== Seattle Pacific"           +$my_spu/INBOX \
     "   Archive"                   +$my_spu/archive \
     "   Drafts"                    +$my_spu/drafts \
     "   Junk"                      +$my_spu/junk \
     "   Sent"                      +$my_spu/sent \
     "   Trash"                     +$my_spu/trash \
     "== Shadow53"                  +$my_sh53shadow53/INBOX \
     "   Archive"                   +$my_sh53shadow53/archive \
     "   Drafts"                    +$my_sh53shadow53/drafts \
     "   Junk"                      +$my_sh53shadow53/junk \
     "   Sent"                      +$my_sh53shadow53/sent \
     "   Trash"                     +$my_sh53shadow53/trash \
     "== Shadow53 Admin"            +$my_sh53admin/INBOX \
     "   Archive"                   +$my_sh53admin/archive \
     "   Drafts"                    +$my_sh53admin/drafts \
     "   Junk"                      +$my_sh53admin/junk \
     "   Sent"                      +$my_sh53admin/sent \
     "   Trash"                     +$my_sh53admin/trash \
     "== Shadow53 Personal"         +$my_sh53personal/INBOX \
     "   Dice"                      +$my_sh53personal/dice \
     "   Indeed"                    +$my_sh53personal/indeed \
     "   LinkedIn"                  +$my_sh53personal/linkedin \
     "   Archive"                   +$my_sh53personal/archive \
     "   Drafts"                    +$my_sh53personal/drafts \
     "   Junk"                      +$my_sh53personal/junk \
     "   Sent"                      +$my_sh53personal/sent \
     "   Trash"                     +$my_sh53personal/trash \
     "== Vivaldi"                   +$my_vivaldi/INBOX \
     "   Archive"                   +$my_vivaldi/archive \
     "   Drafts"                    +$my_vivaldi/drafts \
     "   Junk"                      +$my_vivaldi/junk \
     "   Sent"                      +$my_vivaldi/sent \
     "   Trash"                     +$my_vivaldi/trash

# GPG/PGP 
#set pgp_sign_as = 2F283D0D
#set crypt_use_gpgme = yes
#set crypt_autosign = no
#set crypt_verify_sig = yes
#set crypt_replysign = yes
#set crypt_replyencrypt = yes
#set crypt_replysignencrypted = yes

# some sane vim-like keybindings
bind index,pager k previous-entry
bind index,pager j next-entry
bind index,pager g noop
bind index,pager \Cu half-up
bind index,pager \Cd half-down
bind pager gg top
bind index gg first-entry
bind pager G bottom
bind index G last-entry

# Sidebar Navigation
bind index,pager <down> sidebar-next
bind index,pager <up> sidebar-prev
bind index,pager <right> sidebar-open

# global index and pager shortcuts
bind index,pager @ compose-to-sender
bind index,pager R group-reply
bind index,pager D purge-message
bind index <tab> sync-mailbox
bind index <space> collapse-thread

# Save all attachments
macro pager S "<pipe-message> ripmime -i - -d ~/Downloads && rm ~/Downloads/textfile*" "Save all non-text attachments using ripmime"
# opening urls with urlscan
macro pager \cb "<pipe-message> urlscan<Enter>" "call urlscan to extract URLs out of a message"
# Sync all email
macro index,pager O "<shell-escape>mbsync -a<enter>" "run mbsync to sync all mail"

# Notifications
#set new_mail_command            = "new-main"
#set resolve                     = "no"

# Paths
set alias_file                  = "~/.config/neomutt/aliases"
set attach_save_dir             = "~/.cache/neomutt/attachments"
set header_cache                = "~/.cache/neomutt/headers"
set header_cache_backend        = lmdb
set history_file                = "~/.cache/neomutt/history"
set message_cachedir            = "~/.cache/neomutt/messages"
set certificate_file            = "~/.config/neomtt/certificates"
set mailcap_path                = "~/.config/neomutt/mailcap"
set tmpdir                      = "~/.cache/neomutt/tmp"

folder-hook $my_bnadmin/*        'source ~/.config/neomutt/folder-hooks/bluestnight_admin'
folder-hook $my_bnshadow53/*     'source ~/.config/neomutt/folder-hooks/bluestnight_shadow53'
folder-hook $my_mnbadmin/*       'source ~/.config/neomutt/folder-hooks/mnbryant_admin'
folder-hook $my_mnbaccounts/*    'source ~/.config/neomutt/folder-hooks/mnbryant_folders'
folder-hook $my_mnbpersonal/*    'source ~/.config/neomutt/folder-hooks/mnbryant_personal'
folder-hook $my_sh53admin/*      'source ~/.config/neomutt/folder-hooks/shadow53_admin'
folder-hook $my_sh53personal/*   'source ~/.config/neomutt/folder-hooks/shadow53_personal'
folder-hook $my_sh53shadow53/*   'source ~/.config/neomutt/folder-hooks/shadow53_shadow53'
folder-hook $my_google/*         'source ~/.config/neomutt/folder-hooks/google'
folder-hook $my_outlook          'source ~/.config/neomutt/folder-hooks/outlook_linuxer53'
folder-hook $my_outlookalt       'source ~/.config/neomutt/folder-hooks/outlook_laugh'
folder-hook $my_outlook_sh53     'source ~/.config/neomutt/folder-hooks/outlook_shadow53'
folder-hook $my_spu              'source ~/.config/neomutt/folder-hooks/seattle_pacific'
folder-hook $my_vivaldi          'source ~/.config/neomutt/folder-hooks/vivaldi'

# Default mailbox
source ~/.config/neomutt/folder-hooks/bluestnight_admin
