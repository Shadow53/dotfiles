if status --is-interactive; and which bw &> /dev/null;
    if [ -z "$BW_SESSION" ];
        echo "BW_SESSION is '$BW_SESSION'"
        read -P "Bitwarden password: " -l -s password
        if ! set -xU BW_SESSION (bw unlock "$password" --raw);
            set -xU BW_SESSION (bw login 'myaccounts+bitwarden@mnbryant.com' "$password" --raw)
        end
    end
end
