if status --is-login
    if which nvim &> /dev/null;
        set -xg EDITOR nvim
    else if which vim &> /dev/null;
        set -xg EDITOR vim
    else if which nano &> /dev/null;
        set -xg EDITOR nano
    else if which ee &> /dev/null;
        set -xg EDITOR ee
    else if which vi &> /dev/null;
        set -xg EDITOR vi
    end
end
