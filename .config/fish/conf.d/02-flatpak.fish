if status --is-login;
    if which flatpak &> /dev/null;
        set -e G_MESSAGES_DEBUG
        set -l xdhome "$HOME/.local/share"
        set -q XDG_DATA_HOME; and set -l xdhome "$XDG_DATA_HOME"
        set --path new_dirs "$xdhome/flatpak"
        flatpak --installations | while read flat_path;
            set --path --append new_dirs $flat_path
        end
        
        for install_path in $new_dirs;
            set share_path $install_path/exports/share
            switch ":$XDG_DATA_DIRS:"
                case "*:$share_path:*"
                    :
                case "*:$share_path/:*"
                    :
                case '*'
                    set --path --prepend -g -x XDG_DATA_DIRS $share_path
            end
        end
    end
end
