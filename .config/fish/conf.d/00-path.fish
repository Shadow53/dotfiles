if status --is-login    
    if test -d "/usr/local/bin"; and not contains "/usr/local/bin" $PATH;
        set -x --path --prepend PATH /usr/local/bin
    end

    set NPM_PACKAGES "$HOME/.npm-packages"

    if test -d "$NPM_PACKAGES";
        set -xg --path --append PATH "$NPM_PACKAGES/bin"
        set -xg --path NODE_PATH "$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
        if test -d "$NPM_PACKAGES/share/man";
            set -xg --path MANPATH "$NPM_PACKAGES/share/man:(manpath)"
        end
    end

    set -xg GOPATH "$HOME/.go"

    if test -d "/usr{$HOME}";
        set -xg GOPATH "/usr{$HOME}"
    end

    set -l pathlist "$HOME/.local/bin" "$HOME/bin" "$HOME/.bin" "$HOME/.luarocks/bin" "$HOME/.cargo/bin" "$HOME/.gem/ruby/2.7.0/bin" "$GOPATH/bin"

    for folder in $pathlist;
        if test ! -z "$folder"; and test -d "$folder"; and not contains "$folder" $PATH;
            set -xg --append PATH "$folder"
        end
    end
end
