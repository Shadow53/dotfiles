if status --is-login;
    set -q XDG_DATA_HOME; or set -g -x XDG_DATA_HOME "$HOME/.local/share"
    set -q XDG_CONFIG_HOME; or set -g -x XDG_CONFIG_HOME "$HOME/.config"
    set -q XDG_CACHE_HOME; or set -g -x XDG_CACHE_HOME "$HOME/.cache"

    set -q XDG_DATA_DIRS; or set -g -x --path XDG_DATA_DIRS /usr/local/share/ /usr/share/
    set -q XDG_CONFIG_DIRS; or set -g -x --path XDG_CONFIG_DIRS /etc/xdg
end
