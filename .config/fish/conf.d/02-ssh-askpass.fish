if status --is-login;
    set bsd_seahorse 
    set linux_seahorse 

    set askpasses \
        "/usr/local/lib/seahorse/seahorse-ssh-askpass" \
        "/usr/libexec/seahorse/ssh-askpass" \
        "/usr/bin/ksshaskpass" \
        "/usr/local/bin/ksshaskpass" \
        "/usr/libexec/ssh/ksshaskpass" 

    for askpass in $askpasses;
        if test -f "$askpass";
            set -xg SSH_ASKPASS "$askpass"
            break
        end
    end
end
