if functions | ! grep -q '^fisher$';
    if ! set -q FISHER_INSTALLING_FISHER;
        set -gx FISHER_INSTALLING_FISHER true
        curl -sL git.io/fisher | source && fisher install jorgebucaran/fisher
        set -u FISHER_INSTALLING_FISHER
    end
end
