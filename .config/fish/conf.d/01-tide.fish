if functions | ! grep -q '^tide$';
    if ! set -q FISHER_INSTALLING_TIDE;
        set -gx FISHER_INSTALLING_TIDE true
        fisher install ilancosman/tide
        set -u FISHER_INSTALLING_TIDE
    end
end
