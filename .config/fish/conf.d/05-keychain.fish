set -gx SHELL $__fish_bin_dir/fish
if status --is-interactive; and which keychain &> /dev/null
    keychain --eval --ignore-missing --quiet --agents ssh | source

    for kf in azure borgbase github gitlab home id_rsa ramnode
        if [ -f "$HOME/.ssh/$kf" ];
            set name (echo 'echo $pass_name_'$kf | .)
            set file ~/.ssh/{$kf}
            if ssh-add -L | cut -d ' ' -f -2 | ! grep -q (cat {$file}.pub | cut -d ' ' -f -2);
                env DISPLAY=1 SSH_ASKPASS="$HOME/.bin/pw-askpass" ssh-add ~/.ssh/$kf < /dev/null
            end
        end
    end
end
