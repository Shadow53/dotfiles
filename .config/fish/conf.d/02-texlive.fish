if status --is-login
    if test -d "$HOME/.texmf";
        set -xg TEXMFHOME "$HOME/.texmf"
    end

    set -xg LANG en_US.UTF-8
    set -xg MM_CHARSET UTF-8

    if test -d "$HOME/.local/texlive";
        set -xg TEXDIR "$HOME/.local/texlive/2020"
        set -xg TEXMFROOT "$HOME/.local/texlive/2020"
        set -xg TEXMFLOCAL "$HOME/.local/texlive/texmf-local"
        set -xg TEXMFSYSCONFIG "$HOME/.local/texlive/2020/texmf-config"
        set -xg TEXMFSYSVAR "$HOME/.local/texlive/2020/texmf-var"
        set -xg TEXMFVAR "$TEXMFSYSVAR"
        set -xg TEXMFCONFIG "$TEXMFSYSCONFIG"
    end
end
