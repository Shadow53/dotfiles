if &compatible
    set nocompatible
endif

syntax on

if &shell =~# 'fish$'
    set shell=sh
endif

if &term =~# '256color' && ( &term =~# '^screen'  || &term =~# '^tmux' )
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    if has("termguicolors")
        set termguicolors
    endif
endif

set undodir=$XDG_DATA_HOME/vim/undo
call mkdir(&undodir, 'p')

set guifont=Hack:h9
set linespace=4
set shortmess-=F
set modeline
set undofile

augroup undof
    autocmd!
    autocmd BufWritePre /tmp/* setlocal noundofile
augroup END

let g:lsp_provider = 'vimlsp'

let s:installed_minpac = 0
if has('nvim')
    if !isdirectory($HOME . '/.config/nvim/pack/minpac/opt/minpac') && executable('git')
        ! git clone https://github.com/k-takata/minpac.git ~/.config/nvim/pack/minpac/opt/minpac
        let s:installed_minpac = 1
    endif
else
    if !isdirectory($HOME . '/.vim/pack/minpac/opt/minpac') && executable('git')
        ! git clone https://github.com/k-takata/minpac.git ~/.vim/pack/minpac/opt/minpac
        let s:installed_minpac = 1
    endif
endif

packadd minpac
if exists('g:loaded_minpac')
    call minpac#init()
    call minpac#add('k-takata/minpac', {'type': 'opt'})

    let g:setting_minpac = 1
    runtime! config/*.vim

    command! PackUpdate packadd minpac | call minpac#update('', {'do': 'call minpac#status()'})
    command! PackClean  packadd minpac | call minpac#clean()
    command! PackStatus packadd minpac | call minpac#status()

    if s:installed_minpac
        PackUpdate
    endif
endif

filetype on
    let g:setting_minpac = 0
filetype plugin indent on
set encoding=utf-8
set textwidth=120

" Load settings and such, now that plugins are loaded
let g:setting_minpac = 0
runtime! config/*.vim

" Default file settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set number

if IsTmux()
    if has('nvim')
        set guicursor=
    endif
endif
