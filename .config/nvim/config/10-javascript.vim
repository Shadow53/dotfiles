if g:setting_minpac
    call minpac#add('pangloss/vim-javascript', { 'type': 'opt' })
    call minpac#add('MaxMEllon/vim-jsx-pretty', { 'type': 'opt' })
    call minpac#add('elzr/vim-json', { 'type': 'opt' })
else
    autocmd FileType javascript,javascript.jsx,javascriptreact packadd vim-javascript
    autocmd FileType javascript.jsx,javascriptreact packadd vim-jsx-pretty
    autocmd FileType json packadd vim-json

    autocmd FileType json call NPMInstall('jsonlint', 'jsonlint')
    autocmd FileType javascript,javascript.jsx,javascriptreact call NPMInstall('eslint', 'eslint')
    autocmd FileType javascript,javascript.jsx,javascriptreact \
        call NPMInstall('typescript-language-server', 'typescript typescript-language-server')

    if executable('jq')
        autocmd FileType json set equalprg=jq
    endif

    if g:lsp_provider ==# 'ale'
        autocmd FileType json let b:ale_linters = ['jsonlint']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('typescript-language-server')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'javascript support using typescript-language-server',
        "        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'typescript-language-server --stdio']},
        "        \ 'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'package.json'))},
        "        \ 'whitelist': ['javascript', 'javascript.jsx', 'javascriptreact', 'typescript', 'typescript.tsx'],
        "        \ })
        "elseif executable('eslint-language-server')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'javascript support using eslint-language-server',
        "        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'eslint-language-server --stdio']},
        "        \ 'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'package.json'))},
        "        \ 'whitelist': ['javascript', 'javascript.jsx', 'javascriptreact', 'typescript', 'typescript.tsx'],
        "        \ })
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType json call CocCondInstall('', 'coc-json')
        autocmd FileType javascript,javascript.jsx,javascriptreact call CocCondInstall('', 'coc-eslint')
        autocmd FileType javascript,javascript.jsx,javascriptreact call CocCondInstall('', 'coc-tsserver')
    endif
endif
