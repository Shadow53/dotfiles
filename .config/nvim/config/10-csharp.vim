if g:setting_minpac
    call minpac#add('junegunn/fzf', { 'do': { -> fzf#install() } })
    call minpac#add('OmniSharp/Omnisharp-vim', { 'type': 'start' })
    call minpac#add('nickspoons/vim-sharpenup', { 'type': 'start' })
else
    "autocmd FileType cs packadd Omnisharp-vim
    "autocmd FileType cs packadd vim-sharpenup

    let g:OmniSharp_server_stdio = 1
    let g:OmniSharp_loglevel = 'debug'

    let g:OmniSharp_popup_position = 'peek'
    if has('nvim')
        let g:OmniSharp_popup_options = {
            \ 'winhl': 'Normal:NormalFloat'
            \}
    else
        let g:OmniSharp_popup_options = {
            \ 'highlight': 'Normal',
            \ 'padding': [0, 0, 0, 0],
            \ 'border': [1]
            \}
    endif
    let g:OmniSharp_popup_mappings = {
        \ 'sigNext': '<C-n>',
        \ 'sigPrev': '<C-p>',
        \ 'pageDown': ['<C-f>', '<PageDown>'],
        \ 'pageUp': ['<C-b>', '<PageUp>']
        \}

endif
