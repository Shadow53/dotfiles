if !g:setting_minpac
    autocmd FileType c,cpp,objc,objcpp,cc packadd termdebug

    if executable('clang-format')
        autocmd FileType c,cpp,objc,objcpp,cc set equalprg=clang-format
    endif

    if g:lsp_provider ==# 'ale'
        autocmd FileType c,cpp,objc,objcpp,cc let b:ale_linters = ['ccls', 'clangd', 'cppcheck', 'clang']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('ccls')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'ccls',
        "        \ 'cmd': {server_info->['ccls']},
        "        \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
        "        \ 'initialization_options': {},
        "        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
        "        \ })
        "elseif executable('clangd')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'clangd',
        "        \ 'cmd': {server_info->['clangd', '-background-index']},
        "        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
        "        \ })
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType c,cpp,objc,objcpp,cc call CocCondInstall('clangd', 'coc-clangd')
    endif
endif
