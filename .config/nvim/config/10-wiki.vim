if g:setting_minpac
    call minpac#add('vimwiki/vimwiki')
else
    "let g:vimwiki_list = [{'path': '~/vimwiki/',
    "                  \ 'syntax': 'markdown', 'ext': '.md'}]
endif
