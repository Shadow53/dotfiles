if g:setting_minpac
    call minpac#add('ledger/vim-ledger', { 'type': 'opt' })
else
    autocmd FileType ledger packadd vim-ledger
endif
