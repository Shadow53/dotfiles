" Syntastic uses tidy (HTMLTidy) - make sure it is installed
if g:setting_minpac
    call minpac#add('othree/html5.vim', { 'type': 'opt' })
    call minpac#add('mattn/emmet-vim', { 'type': 'opt' })

    if g:lsp_provider ==# 'vimlsp'
        call minpac#add('prabirshrestha/asyncomplete-emmet.vim',{ 'type': 'opt' })
    endif
else
    " Enable tag autocompletion
    autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
    autocmd FileType html packadd html5.vim
    autocmd FileType html packadd emmet-vim
    autocmd FileType html packadd asyncomplete-emmet.vim
    autocmd FileType html call NPMInstall('html-languageserver', 'vscode-html-languageserver-bin')

    if g:lsp_provider ==# 'ale'
        autocmd FileType html let b:ale_linters = ['tidy']
    elseif g:lsp_provider ==# 'vimlsp'
        autocmd FileType html au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#emmet#get_source_options({
            \ 'name': 'emmet',
            \ 'whitelist': ['html'],
            \ 'completor': function('asyncomplete#sources#emmet#completor'),
            \ }))
        "if executable('html-languageserver')                         
        "    au User lsp_setup call lsp#register_server({               
        "        \ 'name': 'html-languageserver',                     
        "        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'html-languageserver --stdio']},
        "        \ 'whitelist': ['html'],                             
        "    \ })                                                       
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType html call CocCondInstall('', 'coc-html')
        autocmd FileType html call CocCondInstall('', 'coc-emmet')
    endif
endif
