if g:setting_minpac
    call minpac#add('stanangeloff/php.vim', { 'type': 'opt' })
else
    autocmd FileType php packadd php.vim
endif
