" ALE should handle CSS syntax automatically 
if g:setting_minpac
    call minpac#add('hail2u/vim-css3-syntax', { 'type': 'opt' })
else
    autocmd FileType css packadd vim-css3-syntax
    autocmd FileType css,less,sass,scss call NPMInstall('stylelint', 'stylelint')
    autocmd FileType css,less,sass,scss call NPMInstall('css-languageserver', 'vscode-css-languageserver-bin')
    autocmd FileType css setlocal iskeyword+=-

    if g:lsp_provider ==# 'ale'
        autocmd FileType css,less,sass,scss let b:ale_linters = ['stylelint']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('css-languageserver')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'css-languageserver',
        "        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'css-languageserver --stdio']},
        "        \ 'whitelist': ['css', 'less', 'sass', 'scss'],
        "        \ })
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType css,less,sass call CocCondInstall('', 'coc-stylelint')
    endif
endif

