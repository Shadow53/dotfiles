if g:setting_minpac
    call minpac#add('ekalinin/dockerfile.vim', { 'type': 'opt' })
else
    autocmd FileType dockerfile packadd dockerfile.vim 
    autocmd FileType dockerfile NPMInstall('docker-langserver', 'dockerfile-language-server-nodejs')

    if g:lsp_provider ==# 'vimlsp'
        "if executable('docker-langserver')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'docker-langserver',
        "        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'docker-langserver --stdio']},
        "        \ 'whitelist': ['dockerfile'],
        "        \ })
        "endif
    endif
endif
