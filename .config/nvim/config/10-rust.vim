if g:setting_minpac
    call minpac#add('rust-lang/rust.vim', { 'type': 'opt' })
else
    set hidden
    autocmd FileType rust packadd rust.vim
    autocmd FileType rust packadd termdebug
    if g:lsp_provider ==# 'ale'
        autocmd FileType rust let g:ale_rust_cargo_use_clippy = 1
        autocmd FileType rust let b:ale_linters = ['analyzer', 'cargo']
        autocmd FileType rust let b:ale_fixers = ['rustfmt']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('rust-analyzer')
        "    augroup vim_lsp_rust_analyzer
        "        au User lsp_setup call lsp#register_server({
        "            \ 'name': 'rust-analyzer',
        "            \ 'cmd': {server_info->['rust-analyzer']},
        "            \ 'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'Cargo.toml'))},
        "            \ 'whitelist': ['rust'],
        "            \ })
        "        "autocmd User lsp_setup call s:register_command()
        "    augroup END

        "    "function! s:rust_analyzer_apply_source_change(context)
        "    "    let l:command = get(a:context, 'command', {})

        "    "    let l:arguments = get(l:command, 'arguments', [])
        "    "    let l:argument = get(l:arguments, 0, {})

        "    "    let l:workspace_edit = get(l:argument, 'workspaceEdit', {})
        "    "    if !empty(l:workspace_edit)
        "    "        call lsp#utils#workspace_edit#apply_workspace_edit(l:workspace_edit)
        "    "    endif

        "    "    let l:cursor_position = get(l:argument, 'cursorPosition', {})
        "    "    if !empty(l:cursor_position)
        "    "        call cursor(lsp#utils#position#lsp_to_vim('%', l:cursor_position))
        "    "    endif
        "    "endfunction

        "    "let s:setup = 0

        "    "function! s:register_command()
        "    "    if s:setup == 1
        "    "        return
        "    "    endif
        "    "    let s:setup = 1
        "    "    augroup vimlsp_settings_rust_analyzer
        "    "        au!
        "    "    augroup END
        "    "    if exists('*lsp#register_command')
        "    "        call lsp#register_command('rust-analyzer.applySourceChange', function('s:rust_analyzer_apply_source_change'))
        "    "    endif
        "    "endfunction
        "endif 
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType rust call CocCondInstall('', 'coc-rust-analyzer')
    endif
endif
