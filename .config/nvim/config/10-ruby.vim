if g:setting_minpac
    call minpac#add('tpope/vim-rails', { 'type': 'opt' })
    call minpac#add('vim-ruby/vim-ruby', { 'type': 'opt' })
else
    autocmd FileType ruby packadd vim-ruby
    autocmd FileType ruby packadd vim-rails
endif
