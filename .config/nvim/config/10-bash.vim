if !g:setting_minpac
    autocmd FileType bash call NPMInstall('bash-language-server', 'bash-language-server')

    if g:lsp_provider ==# 'ale'
        autocmd FileType bash let b:ale_linters = ['language-server', 'shellcheck']
        autocmd FileType sh   let b:ale_linters = ['shellcheck']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('bash-language-server')
        "    augroup LspBash
        "        autocmd!
        "        autocmd User lsp_setup call lsp#register_server({
        "            \ 'name': 'bash-language-server',
        "            \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
        "            \ 'allowlist': ['sh'],
        "            \ })
        "    augroup END
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType bash,sh call CocCondInstall('', 'coc-sh')
    endif
endif
