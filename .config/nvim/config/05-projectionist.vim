if g:setting_minpac
    call minpac#add('tpope/vim-projectionist', { 'type': 'start' })
else
    "autocmd User ProjectionistActivate call s:activate()

    "function! s:activate() "abort
    "    echom 'plugins: ' . join(projectionist#query('vim-plugins'))
    "    echom 'filetypes: ' . join(projectionist#query('vim-filetypes'))
    "    for [root, value] in projectionist#query('vim-plugins')
    "        echom 'found projection for ' . root . ': ' . join(value)
    "        for plug in value
    "            echom 'starting plugin ' . plug
    "            silent exec 'packadd ' . plug
    "        endfor
    "        break
    "    endfor
    "    for [root, value] in projectionist#query('vim-filetypes')
    "        echom 'found projection for ' . root . ': ' . join(value)
    "        let l:fts = join(value, '.')
    "        echom 'setting filetype to ' . l:fts
    "        silent exec 'set ft=' . l:fts
    "        break
    "    endfor
    "endfunction
endif
