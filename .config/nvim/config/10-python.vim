if !g:setting_minpac
    autocmd FileType python call PipInstall('autopep8', '')
    autocmd FileType python set equalprg=autopep8\ --aggressive\ --aggressive\ - 
    autocmd FileType python set formatprg=autopep8\ --aggressive\ --aggressive\ - 
    if g:lsp_provider ==# 'ale'
        autocmd FileType python let b:ale_linters = ['flake8']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('jedi-language-server')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'jedi-language-server',
        "        \ 'cmd': {server_info->['jedi-language-server']},
        "        \ 'whitelist': ['python'],
        "        \ })
        "elseif executable('pyls')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'pyls',
        "        \ 'cmd': {server_info->['pyls']},
        "        \ 'whitelist': ['python'],
        "        \ })
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType python call CocCondInstall('jedi-language-server', 'coc-jedi')
    endif
endif
