if g:setting_minpac
    call minpac#add('chr4/nginx.vim', { 'type': 'opt' })
else
    autocmd FileType nginx packadd nginx.vim
    au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/*,/usr/local/etc/nginx/* if &ft == '' | setfiletype nginx | endif
endif
