if g:setting_minpac
    call minpac#add('neomake/neomake', { 'type': 'start' })
    if has('nvim-0.5.0')
        call minpac#add('neovim/nvim-lspconfig')
    else
        if g:lsp_provider ==# 'ale'
            call minpac#add('dense-analysis/ale', { 'type': 'start' })
            if has('nvim')
                call minpac#add('Shougo/deoplete.nvim', { 'type': 'start', 'do': 'UpdateRemotePlugins' })
            else
                call minpac#add('roxma/nvim-yarp', { 'type': 'start' })
                call minpac#add('roxma/vim-hug-neovim-rpc', { 'type': 'start' })
                call minpac#add('Shougo/deoplete.nvim', { 'type': 'start' })
            endif
        elseif g:lsp_provider ==# 'vimlsp'
            call minpac#add('prabirshrestha/vim-lsp', { 'type': 'start' })
            call minpac#add('mattn/vim-lsp-settings', { 'type': 'start' })
            call minpac#add('prabirshrestha/asyncomplete.vim', { 'type': 'start' })
            call minpac#add('prabirshrestha/asyncomplete-lsp.vim', { 'type': 'start' })
            call minpac#add('thomasfaingnaert/vim-lsp-snippets', { 'type': 'start' })
            call minpac#add('thomasfaingnaert/vim-lsp-neosnippet', { 'type': 'start' })
            call minpac#add('prabirshrestha/asyncomplete-file.vim', { 'type': 'start' })
        elseif g:lsp_provider ==# 'coc'
            call minpac#add('neoclide/coc.nvim', { 'branch': 'release', 'type': 'start' })
        endif
    endif
else
    set hidden
    function! EnableNeomake()
        NeomakeEnableBuffer
        " Full config: when writing or reading a buffer, and on changes in insert and
        " normal mode (after 500ms; no delay when writing).
        call neomake#configure#automake('nrwi', 500)
    endfunction

    function! DisableNeomake()
        NeomakeDisableBuffer
    endfunction

    if g:lsp_provider ==# 'ale'
        let g:ale_sign_column_always = 1
        let g:airline#extensions#ale#enabled = 1
        let g:ale_completion_enabled = 0
        let g:deoplete#enable_at_startup = 1
        let g:ale_fixers = { '*': ['trim_whitespace'] }

        packadd deoplete.nvim
        call deoplete#custom#option('sources', {'_': [
            \ 'ale', 'around', 'buffer', 'file', 'neosnippet'
            \ ]})
        call deoplete#custom#source('ale', 'rank', 999)

        nmap <silent> [W <Plug>(ale_first)
        nmap <silent> [w <Plug>(ale_previous)
        nmap <silent> ]w <Plug>(ale_next)
        nmap <silent> ]W <Plug>(ale_last)
        
        augroup configure_projects
            autocmd!
            autocmd User ProjectionistActivate call s:linters()
        augroup END
        function! s:linters() abort
            let l:linters = projectionist#query('linters')
            if len(l:linters) > 0
                let b:ale_linters = {&filetype: l:linters[0][1]}
            endif
        endfunction
    elseif g:lsp_provider ==# "vimlsp"
        let g:deoplete#enable_at_startup = 0
        let g:lsp_signs_enabled = 1
        let g:lsp_highlight_references_enabled = 1
        let g:asyncomplete_auto_popup = 1
        let g:asyncomplete_auto_completeopt = 0
        set completeopt=menuone,noinsert,noselect,preview

        " Tab completion
        inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
        inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
        inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"

        au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
            \ 'name': 'file',
            \ 'whitelist': ['*'],
            \ 'priority': 10,
            \ 'completor': function('asyncomplete#sources#file#completor')
            \ }))
    elseif g:lsp_provider ==# "coc"
        let g:deoplete#enable_at_startup = 0
    endif
endif
