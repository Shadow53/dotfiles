function! IsTmux()
    return exists('$TMUX')
endfunction

function! GetParentDir()
    return expand('%:p:h:t')
endfunction

function! CocCondInstall(bin, plug)
    if g:lsp_provider ==# 'coc'
        if a:bin ==# '' || executable(a:bin)
            if !isdirectory($HOME . '/.config/coc/extensions/node_modules/' . a:plug)
                execute 'CocInstall ' . a:plug
            endif
        endif
    endif
endfunction

function! CargoInstall(bin, crate, repo)
    if executable('cargo') && !executable(a:bin)
        if exists(a:repo) && a:repo !=# ''
            silent execute '! cargo install --git ' . a:repo
        else
            silent execute '! cargo install ' . a:crate
        endif
    endif
endfunction

function! GoInstall(bin, path)
    if executable('go') && !executable(a:bin)
        silent execute '! go get -u ' . a:path
    endif
endfunction

function! NPMInstall(bin, pkg)
    if !exists('a:pkg') || a:pkg ==# ''
        let a:pkg = a:bin
    endif

    if executable('npm') && !executable(a:bin)
        silent execute '! npm install -g -y ' . a:pkg
    endif
endfunction

function! PipInstall(bin, module)
    if !exists('a:module') || a:module ==# ''
        let l:module = a:bin
    else
        let l:module = a:module
    endif

    if executable('pip3')
        let l:pip = 'pip3'
    elseif executable('pip')
        let l:pip = 'pip'
    endif

    if executable('python3')
        let l:python = 'python3'
    elseif executable('python')
        let l:python = 'python'
    endif

    if exists('l:pip')
        silent execute '!' . l:python . ' -c "import ' . a:bin . '"'
        if v:shell_error != 0 && !executable(a:bin)
            silent execute '!' . l:pip . ' install --user --upgrade ' . l:module
        endif
    endif
endfunction

if g:setting_minpac
    "call minpac#add('aperezdc/vim-template', { 'type': 'start' })
    call minpac#add('Shougo/neosnippet.vim', { 'type': 'start' })
    call minpac#add('Shougo/neosnippet-snippets', { 'type': 'start' })
    call minpac#add('vim-airline/vim-airline', { 'type': 'start' })
    call minpac#add('mtth/scratch.vim', { 'type': 'start' })
    call minpac#add('editorconfig/editorconfig-vim', { 'type': 'start' })
    call minpac#add('sunaku/tmux-navigate', { 'type': 'start' })
    call minpac#add('pbrisbin/vim-mkdir', { 'type': 'start' })
    if !has('nvim')
        call minpac#add('rhysd/vim-healthcheck')
    endif

    if g:lsp_provider !=# 'coc' && has('nvim')
        call minpac#add('aurieh/discord.nvim', { 'type': 'start', 'do': ':UpdateRemotePlugins'})
    endif
    " Themes
    " call minpac#add('morhetz/gruvbox', { 'type': 'start' })
    call minpac#add('arcticicestudio/nord-vim', { 'type': 'start' })
else
    let g:email = 'shadow53@shadow53.com'
    let g:neosnippet#snippets_directory='~/.config/nvim/snippets'
    let g:templates_detect_git = 1
    let g:templates_directory = '~/.config/nvim/templates'
    let g:templates_use_licensee = 1
    let g:templates_user_variables = [
        \ ['PARENT', 'GetParentDir']
        \ ]

    " colorscheme one
    " let g:airline_theme='one'
    " colorscheme gruvbox
    " let g:airline_theme='gruvbox'
    colorscheme nord
    let g:airline_theme='nord'
    set background=dark

    let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']
    autocmd CompleteDone * call neosnippet#complete_done()

    if has('nvim')
        call PipInstall('neovim', 'neovim')
    endif
    call PipInstall('proselint', 'proselint')

    if g:lsp_provider ==# 'coc'
        autocmd VimEnter call CocCondInstall('discord', 'coc-cord')
        autocmd VimEnter call CocCondInstall('', 'coc-marketplace')
    endif
endif
