if g:setting_minpac
    call minpac#add('jparise/vim-graphql', { 'type': 'opt' })
else
    autocmd FileType graphql,javascript,javascript.jsx,javascriptreact packadd vim-graphql
endif
