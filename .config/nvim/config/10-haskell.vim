" eagletmt/neco-ghc
" ALE should handle haskell linting automatically
if !g:setting_minpac
    call minpac#add('neovimhaskell/haskell-vim', { 'type': 'opt' })
    call minpac#add('alx741/vim-hindent', { 'type': 'opt' })
else
    autocmd FileType haskell packadd haskell-vim
    autocmd FileType haskell packadd vim-hindent

    let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
    let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
    let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
    let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
    let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
    let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
    let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

    if g:lsp_provider ==# 'ale'
        autocmd FileType haskell packadd ale
    elseif g:lsp_provider ==# 'vimlsp'
        if (executable('haskell-language-server-wrapper'))
            au User lsp_setup call lsp#register_server({
            \ 'name': 'haskell-language-server-wrapper',
            \ 'cmd': {server_info->['haskell-language-server-wrapper', '--lsp']},
            \ 'whitelist': ['haskell'],
            \ })
        endif
        "if executable('hie-wrapper')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'lua-lsp',
        "        \ 'cmd': {server_info->['hie', '--lsp']},
        "        \ 'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'stack.yaml'))},
        "        \ 'whitelist': ['haskell'],
        "        \ })
        "endif
    endif
endif
