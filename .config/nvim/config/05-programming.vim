if g:setting_minpac
    call minpac#add('airblade/vim-gitgutter', { 'type': 'start' })
    "call minpac#add('jiangmiao/auto-pairs', { 'type': 'start' })
    "call minpac#add('tpope/vim-surround', { 'type': 'start' })
    call minpac#add('Raimondi/delimitMate', { 'type': 'start' })
    call minpac#add('tpope/vim-fugitive', { 'type': 'start' })
    call minpac#add('vim-test/vim-test', { 'type': 'start' })
    call minpac#add('tpope/vim-dispatch', { 'type': 'start' })
    call minpac#add('mg979/vim-visual-multi', { 'type': 'start' })
else
    if executable('lldb')
        let termdebugger = 'lldb'
    elseif executable('gdb')
        let termdebugger = 'gdb'
    endif

    let test#strategy = "dispatch_background"
    call NPMInstall('prettier', 'prettier')
endif

