if !g:setting_minpac
    autocmd FileType vim call NPMInstall('vimls', 'vim-language-server')
    if g:lsp_provider ==# 'ale'
        autocmd FileType vim let b:ale_linters = ['vimls']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('vim-language-server')
        "    augroup LspVim
        "        autocmd!
        "        autocmd User lsp_setup call lsp#register_server({
        "            \ 'name': 'vim-language-server',
        "            \ 'cmd': {server_info->['vim-language-server', '--stdio']},
        "            \ 'whitelist': ['vim'],
        "            \ 'initialization_options': {
        "            \   'vimruntime': $VIMRUNTIME,
        "            \   'runtimepath': &rtp,
        "            \ }})
        "    augroup END
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType vim call CocCondInstall('', 'coc-vimlsp')
    endif
endif
