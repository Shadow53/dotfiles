if g:setting_minpac
    call minpac#add('pearofducks/ansible-vim', { 'type': 'opt' })
else
    autocmd FileType yaml packadd ansible-vim
    if g:lsp_provider ==# 'ale'
        autocmd FileType yaml call PipInstall('yamllint', 'yamllint')
        autocmd FileType yaml let b:ale_linters = ['yamllint']
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType yaml call CocCondInstall('', 'coc-yaml')
    endif
endif
