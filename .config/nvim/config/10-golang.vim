" deoplete-plugins/deoplete-go {'build': 'make'}
" Maybe a Go language server? Apparently Google is making one
" fatih/vim-go

if g:setting_minpac
    "if has('nvim') && g:lsp_provider ==# 'ale'
    "    call minpac#add('deoplete-plugins/deoplete-go', { 'type': 'opt', 'do': 'gmake' })
    "endif
    call minpac#add('fatih/vim-go', { 'type': 'start' })
else
    command! GoTemplate call SyntaxGoTemplate()

    function! SyntaxGoTemplate()
        packadd vim-go
        if &filetype ==# 'html'
            silent exec 'set filetype=gohtmltmpl.' . &filetype
        else
            silent exec 'set filetype=gotexttmpl.' . &filetype
        endif
    endfunction

    " vim-go conflicts with vim-template
    let g:go_template_autocreate = 0
    
    autocmd FileType go packadd vim-go
    autocmd FileType go call GoInstall('gopls', 'golang.org/x/tools/gopls@latest')

    if g:lsp_provider ==# 'ale'
        autocmd FileType go let b:ale_linters = ['gopls', 'goimports', 'golint', 'golangci-lint', 'govet', 'revive']
        autocmd FileType go let b:ale_fixers  = ['goimports', 'remove_trailing_lines', 'trim_whitespace']
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('gopls')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'gopls',
        "        \ 'cmd': {server_info->['gopls']},
        "        \ 'whitelist': ['go'],
        "        \ })
        "    autocmd BufWritePre *.go LspDocumentFormatSync
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType go call CocCondInstall('gopls', 'coc-go')
    endif
endif
