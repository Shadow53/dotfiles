if g:setting_minpac
    call minpac#add('dag/vim-fish', { 'type': 'opt' })
else
    autocmd FileType fish packadd vim-fish
    autocmd FileType fish compiler fish
    autocmd FileType fish set foldmethod=expr
endif
