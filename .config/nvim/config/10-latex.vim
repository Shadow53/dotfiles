if g:setting_minpac
    call minpac#add('lervag/vimtex', { 'type': 'opt' })
    call minpac#add('donRaphaco/neotex', { 'type': 'opt' })
else
    let g:tex_flavor = 'latex'
    autocmd FileType tex,rnoweb packadd vimtex
    autocmd FileType tex,rnoweb packadd neotex
    autocmd FileType tex,rnoweb let g:neotex_enabled = 1
    autocmd FileType tex,rnoweb call CargoInstall('texlab', 'texlab', 'https://github.com/latex-lsp/texlab.git')

    if g:lsp_provider ==# 'ale'
        autocmd FileType tex,rnoweb let b:ale_linters = ['texlab', 'proselint', 'chktex', 'lacheck']
        let g:ale_history_log_output = 1
        autocmd FileType tex,rnoweb call deoplete#custom#var('omni', 'input_patterns', {'tex' : g:vimtex#re#deoplete})
    elseif g:lsp_provider ==# 'vimlsp'
        "if executable('texlab')
        "    au User lsp_setup call lsp#register_server({
        "        \ 'name': 'texlab',
        "        \ 'cmd': {server_info->['texlab']},
        "        \ 'whitelist': ['tex', 'bib', 'sty'],
        "        \ })
        "endif
    elseif g:lsp_provider ==# 'coc'
        autocmd FileType tex,rnoweb call CocCondInstall('', 'coc-texlab')
        autocmd FileType tex,rnoweb call CocCondInstall('', 'coc-vimtex')
    endif
endif
