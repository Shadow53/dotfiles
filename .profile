NPM_PACKAGES="$HOME/.npm-packages"
mkdir -p "$NPM_PACKAGES"

contains() {
    test "${1#*$2}" != "$1"
    return $?
}

# Prioritize port programs over system ones
if [ -d "/usr/local/bin" ] && ! contains "$PATH" "/usr/local/bin"; then
    export PATH=/usr/local/bin:$PATH
fi

if [ -d ~/.npm-packages ]; then
    export PATH="$PATH:$NPM_PACKAGES/bin"
    export NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
    if [ -d ~/.npm-packages/share/man ]; then
        unset MANPATH
        export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
    fi
fi

export GOPATH=~/.go
if [ -d "/usr${HOME}" ]; then
    # Handle non-symlink path to $HOME
    export GOPATH=/usr${HOME}/.go
fi

while IFS= read -r p; do
    if [ ! -z "$p" ] && [ -d "$p" ] && ! contains "$PATH" "$p"; then
        export PATH="$PATH:$p"
    fi
done <<EOF
/nonexistent/usr/local/bin
$HOME/.local/bin
$HOME/bin
$HOME/.bin
$HOME/.luarocks/bin
$HOME/.cargo/bin
$HOME/.gem/ruby/2.6.0/bin
$GOPATH/bin
EOF


if [ -f "/usr/local/lib/seahorse/seahorse-ssh-askpass" ]; then
    export SSH_ASKPASS="/usr/local/lib/seahorse/seahorse-ssh-askpass"
elif [ -f /usr/bin/ksshaskpass ] || [ -f /usr/local/bin/ksshaskpass ]; then
    export SSH_ASKPASS=ksshaskpass
fi

if which kak > /dev/null; then
    export EDITOR=kak
elif which nvim > /dev/null; then
    export EDITOR=nvim
elif which nano > /dev/null; then
    export EDITOR=nano
elif which ee > /dev/null; then
    export EDITOR=ee
elif which vim > /dev/null; then
    export EDITOR=vim
elif which vi > /dev/null; then
    export EDITOR=vi
fi

if [ -d "$HOME/.texmf" ]; then
    export TEXMFHOME="$HOME/.texmf"
fi

export LANG=en_US.UTF-8
export MM_CHARSET=UTF-8

# Run powerline-daemon - don't complain if already running
powerline-daemon -q

#if [ -x /usr/bin/fortune ] ; then /usr/bin/fortune freebsd-tips ; fi

if [ -d "$HOME/.local/texlive" ]; then
    export TEXDIR="$HOME/.local/texlive/2019"
    export TEXMFLOCAL="$HOME/.local/texlive/texmf-local"
    export TEXMFSYSCONFIG="$HOME/.local/texlive/2019/texmf-config"
    export TEXMFSYSVAR="$HOME/.local/texlive/2019/texmf-var"
    export TEXMFVAR="$TEXMFSYSVAR"
    export TEXMFCONFIG="$TEXMFSYSCONFIG"
fi

# include Mycroft commands
[ -e ~/.profile_mycroft ] && source ~/.profile_mycroft

export PATH="$HOME/.cargo/bin:$PATH"
