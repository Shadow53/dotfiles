#!/usr/bin/env bash

. ~/.bin/common-funcs

get_arch() {
    arch="$(uname -m)"
    while true; do
        karch="$1"
        dlarch="$2"
        if [ "$arch" = "$karch" ]; then
            echo "$dlarch"
            return 0
        fi
    done
    return 1
}

WORKDIR="/tmp"
BIN_DIR="$HOME/.bin"

ALTERAEON_LINUX_URL="http://www.alteraeon.com/AlterAeonLauncherLinux64.bin"
ALTERAEON_BIN_SRC="AlterAeonLauncherLinux64.bin"
ALTERAEON_BIN_TARGET="alteraeon"

HUGO_VERSION="0.74.3"
HUGO_ARCH="$(get_arch "aarch64" "ARM64" "x86_64" "64bit")"
HUGO_LINUX_URL="https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-${HUGO_ARCH}.tar.gz"
HUGO_FREEBSD_URL="https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_FreeBSD-${HUGO_ARCH}.tar.gz"
HUGO_BIN_SRC="hugo"
HUGO_BIN_TARGET="hugo"

ELVISH_VERSION="0.13"
ELVISH_LINUX_URL="https://dl.elv.sh/linux-amd64/elvish-v${ELVISH_VERSION}.tar.gz"
ELVISH_BIN_SRC="elvish-v${ELVISH_VERSION}"
ELVISH_BIN_TARGET="elvish"

EXERCISM_VERSION="3.0.13"
EXERCISM_ARCH="$(get_arch "aarch64" "armv6" "x86_64" "x86_64")"
EXERCISM_LINUX_URL="https://github.com/exercism/cli/releases/download/v${EXERCISM_VERSION}/exercism-linux-${EXERCISM_ARCH}.tgz"
EXERCISM_FREEBSD_URL="https://github.com/exercism/cli/releases/download/v${EXERCISM_VERSION}/exercism-freebsd-${EXERCISM_ARCH}.tgz"
EXERCISM_BIN_SRC="exercism"
EXERCISM_BIN_TARGET="exercism"

ADB_LINUX_URL="https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
ADB_BIN_SRC=platform-tools/adb
ADB_BIN_TARGET="adb"

FASTBOOT_LINUX_URL="https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
FASTBOOT_BIN_SRC=platform-tools/fastboot
FASTBOOT_BIN_TARGET="fastboot"

MKE2FS_LINUX_URL="https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
MKE2FS_BIN_SRC="platform-tools/mke2fs"
MKE2FS_BIN_TARGET="mke2fs"

SHADOWHOSTS_FREEBSD_URL="https://dl.shadow53.com/shadowhosts/freebsd/shadowhosts.amd64"
SHADOWHOSTS_LINUX_URL="https://dl.shadow53.com/shadowhosts/linux/shadowhosts.amd64"
SHADOWHOSTS_BIN_SRC="shadowhosts.amd64"
SHADOWHOSTS_BIN_TARGET="shadowhosts"

THEDARKMOD_LINUX_URL="http://darkmod.taaaki.za.net/release/tdm_update_linux.zip"
THEDARKMOD_BIN_SRC="tdm_update.linux"
THEDARKMOD_BIN_TARGET="tdm_update"

JETBRAINS_VERSION="1.17.6856"
JETBRAINS_LINUX_URL="https://download.jetbrains.com/toolbox/jetbrains-toolbox-${JETBRAINS_VERSION}.tar.gz"
JETBRAINS_BIN_SRC="jetbrains-toolbox-${JETBRAINS_VERSION}/jetbrains-toolbox"
JETBRAINS_BIN_TARGET="jetbrains-toolbox"

TEXLAB_VERSION="v2.2.0"

RUST_ANALYZER_VERSION="2020-09-07"
RUST_ANALYZER_LINUX_URL="https://github.com/rust-analyzer/rust-analyzer/releases/download/${RUST_ANALYZER_VERSION}/rust-analyzer-linux"
RUST_ANALYZER_BIN_SRC="rust-analyzer-linux"
RUST_ANALYZER_BIN_TARGET="rust-analyzer"

declare -a progs
#progs=(ALTERAEON ANTIBODY HUGO ELVISH EXERCISM ADB FASTBOOT MKE2FS SHADOWHOSTS THEDARKMOD)
#progs=(ADB FASTBOOT MKE2FS EXERCISM HUGO SHADOWHOSTS JETBRAINS TEXLAB RUST_ANALYZER)
progs=(EXERCISM SHADOWHOSTS JETBRAINS TEXLAB RUST_ANALYZER)

for p in "${progs[@]}"; do
    if which cargo &> /dev/null && [ "$p" = "TEXLAB" ]; then
        cargo install --git https://github.com/latex-lsp/texlab.git --tag "$TEXLAB_VERSION"
        continue
    fi
    
    unset url
    if is_linux; then
        url=${p}_LINUX_URL
    elif is_freebsd; then
        url=${p}_FREEBSD_URL
    fi

    [[ -z "${!url}" ]] && continue

    #ver=${p}_VERSION
    archive=${WORKDIR}/${!url##*/}
    bin=${p}_BIN_TARGET
    src=${p}_BIN_SRC
    
    bin=${BIN_DIR}/${!bin}
    src=${WORKDIR}/${!src}

    rm -f "${archive}"
    download "${!url}" "${archive}"
    if [[ "$archive" == *".tar.gz" ]] || [[ "$archive" == *".tgz" ]]; then
        tar -zxvf "${archive}" -C "${WORKDIR}"
    elif [[ "$archive" == *".zip" ]]; then
        unzip -q -d "${WORKDIR}" -o "$archive"
    fi
    mv "$src" "$bin"
    chmod +x "$bin"
done
