#!/bin/sh

mpd_packages="musicpd musicpc"
#retroarch_packages="libretro-core-info libretro-shaders-glsl libretro-shaders-slang libretro retroarch"
retroarch_packages="libretro-core-info libretro-shaders-glsl libretro-shaders-slang libretro-beetle_bsnes libretro-beetle_gba libretro-genesis_plus_gx libretro-snes9x libretro-vbanext retroarch"
kodi_packages="kodi kodi-addon-peripheral-joystick"
emulation_packages="dolphin-emu dosbox higan mednafen"
controller_packages="antimicro controllermap"

kodi_rc="/etc/rc.conf.d/kodi"
mpd_rc="/etc/rc.conf.d/musicpd"

mpd_install() {
    sudo pkg uninstall $mpd_packages
    echo 'musicpd_enable="YES"' | sudo tee /etc/rc.conf.d/musicpd > /dev/null

    mkdir -p "$HOME/.mpd/playlists"
    mkdir -p "$HOME/.mpd/cache"
    mkdir -p "$HOME/.mpd/music"

    ln -sf "$HOME/Music"    "$HOME/.mpd/music/music"
    ln -sf "$HOME/Podcasts" "$HOME/.mpd/music/podcasts"

    sudo tee /usr/local/etc/musicpd.conf <<EOF
music_directory			"/usr/home/shadow53/.mpd/music"
playlist_directory              "/usr/home/shadow53/.mpd/playlists"
log_file                        "syslog"
pid_file                        "/usr/home/shadow53/.mpd/pid"
state_file                      "/usr/home/shadow53/.mpd/state"
sticker_file                    "/usr/home/shadow53/.mpd/sticker.sql"

user                            "shadow53"
group                           "shadow53"

bind_to_address                 "any"
port                            "6600"

restore_paused                  "yes"
auto_update                     "yes"
auto_update_depth               "3"

zeroconf_enabled                "yes"
zeroconf_name                   "MPD @ %h"

database {
    plugin "simple"
    path   "/usr/home/shadow53/.mpd/db"
    cache_directory "/usr/home/shadow53/.mpd/cache"
}

input {
    plugin "curl"
}

audio_output {
    type   "oss"
    name   "My OSS Device"
}

audio_output {
    type   "pulse"
    name   "My Pulse Output"
}
EOF
}

musicpd_uninstall() {
    sudo pkg uninstall $mpd_packages
    sudo rm /usr/local/etc/musicpd.conf
    sudo rm /etc/rc.conf.d/musicpd
}

case $1 in
    install|add)
	action="install"
	;;
    uninstall|remove)
	action="remove"
	;;
    *)
	echo "No such action for media: $1" 1>&2
	exit 1
	;;
esac

shift 1

targets="$@"
if [ -z "$targets" ]; then
    targets="musicpd retroarch kodi controller"
fi

for target in $targets; do
    case $target in
        controller)
	    sudo pkg install -y $controller_packages
	    ;;
	kodi)
	    sudo pkg install -y $kodi_packages
	    ;;
	musicpd)
	    mpd_install
	    ;;
	retroarch)
	    sudo pkg install -y $retroarch_packages
	    ;;
	*)
	    echo "No such target for media: $target" 1>&2
	    ;;
    esac
done
