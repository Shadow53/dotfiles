#!/usr/bin/env bash
# SOLARIZED HEX     16/8 TERMCOL   L*A*B     
# --------- ------- ---- --------- ----------
#base03="#2e3440"  # base03    #002b36  8/4 brblack   15 -12 -12
#base02="#073642"  # base02    #073642  0/4 black     20 -12 -12
#base01="#586e75"  # base01    #586e75 10/7 brgreen   45 -07 -07
#base00="#657b83"  # base00    #657b83 11/7 bryellow  50 -07 -07
#base0="#839496"  # base0     #839496 12/6 brblue    60 -06 -03
#base1="#93a1a1"  # base1     #93a1a1 14/4 brcyan    65 -05 -02
#base2="#eee8d5"  # base2     #eee8d5  7/7 white     92 -00  10
#base3="#fdf6e3"  # base3     #fdf6e3 15/7 brwhite   97  00  10
#yellow="#b58900"  # yellow    #b58900  3/3 yellow    60  10  65
#orange="#cb4b16"  # orange    #cb4b16  9/3 brred     50  50  55
#red="#dc322f"  # red       #dc322f  1/1 red       50  65  45
#magenta="#d33682"  # magenta   #d33682  5/5 magenta   50  65 -05
#violet="#6c71c4"  # violet    #6c71c4 13/5 brmagenta 50  15 -45
#blue="#268bd2"  # blue      #268bd2  4/4 blue      55 -10 -45
#cyan="#2aa198"  # cyan      #2aa198  6/6 cyan      60 -35 -05
#green="#859900"  # green     #859900  2/2 green     60 -20  65

#background="#2e3440"
#foreground="#d8dee9"
#cursor="#8fbcbb"
#mouse_background="#3b4252"
#mouse_foreground="#8fbcbb"
#highlight="#3b4252"
#border="#4c566a"
#
#color0="#3b4252"  # black
#color8="#4c566a"  # brblack
#
#color1="#bf616a"     # red
#color9="#bf616a"  # brred
#
#color2="#a3be8c"   # green
#color10="#a3be8c"  # brgreen
#
#color3="#ebcb8b"  # yellow
#color11="#ebcb8b"  # bryellow
#
#color4="#81a1c1"    # blue
#color12="#81a1c1"   # brblue
#
#color5="#b48ead" # magenta
#color13="#b48ead"  # brmagenta
#
#color6="#88c0d0"    # cyan
#color14="#8fbcbb"   # brcyan
#
#color7="#e5e9f0"   # white
#color15="#eceff4"   # brwhite

background="#2e3440"
foreground="#d8dee9"
cursor="#8fbcbb"
mouse_background="#3b4252"
mouse_foreground="#8fbcbb"
highlight="#3b4252"
border="#4c566a"

color0="#3b4252"  # black
color8="#4c566a"  # brblack

color1="#bf616a"     # red
color9="#bf616a"  # brred

color2="#a3be8c"   # green
color10="#a3be8c"  # brgreen

color3="#ebcb8b"  # yellow
color11="#ebcb8b"  # bryellow

color4="#81a1c1"    # blue
color12="#81a1c1"   # brblue

color5="#b48ead" # magenta
color13="#b48ead"  # brmagenta

color6="#88c0d0"    # cyan
color14="#8fbcbb"   # brcyan

color7="#e5e9f0"   # white
color15="#eceff4"   # brwhite

die () {
    echo >&2 "$*"
    exit 1
}

change_color () {
    case $1 in
        color*)
            send_osc 4 "${1#color};$2" ;;
        foreground)
            send_osc 10 "$2" ;;
        background)
            send_osc 11 "$2" ;;
        cursor)
            send_osc 12 "$2" ;;
        mouse_foreground)
            send_osc 13 "$2" ;;
        mouse_background)
            send_osc 14 "$2" ;;
        highlight)
            send_osc 17 "$2" ;;
        border)
            send_osc 708 "$2" ;;
    esac
}

send_escape_sequence () {
    escape_sequence="$1"

  # wrap escape sequence when within a TMUX session
  [ ! -z "$TMUX" ] && escape_sequence="${DSC}tmux;${ESC}${escape_sequence}${ESC}\\"

  printf "${escape_sequence}"
}

send_osc () {
    Ps=$1
    Pt=$2
    command="$OSC$Ps;$Pt$BEL"
    send_escape_sequence $command
}

ESC="\033"
BEL="\007"
DSC="${ESC}P"
OSC="${ESC}]"

colors=( background foreground cursor mouse_background mouse_foreground highlight border color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15 )
color_names=( black red green yellow blue magenta cyan white brblack brred brgreen bryellow brblue brmagenta brcyan brwhite )

init () {
    set_colors
}

unset_colors () {
    for color in ${colors[@]}; do
        unset ${!color}
    done
}

set_colors () {
    for color in ${colors[@]}; do
        [[ ${!color} ]] && change_color "$color" "${!color}"
    done
}

set_colors
