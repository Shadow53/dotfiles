#!/usr/bin/env bash

PODCASTS_DIR="${PODCASTS_DIR:-$HOME/Podcasts}"
PODCASTS_DIR="${PODCASTS_DIR%'/'}"
PLAYER="${PLAYER:-mpv --audio-display=no}"

raw_folders=()
folders=()
episodes=()

flen=0
elen=0

player_exists() {
    which $1 > /dev/null
}

strip_prefix() {
    local prefix="$2"
    if [ ! -z "$prefix" ]; then
        prefix="${prefix}/"
    else
        prefix="${PODCASTS_DIR}/"
    fi
    echo "${1#"$prefix"}"
}

set_folders() {
    unset raw_folders
    unset folders

    while IFS=  read -r -d $'\0'; do
        raw_folders+=("$REPLY")
    done < <(find "$PODCASTS_DIR" -maxdepth 1 -type d -print0)

    i=0
    while [ $i -lt ${#raw_folders[*]} ]; do
        if [ "${raw_folders[$i]}" != "${PODCASTS_DIR}" ] && [[ "$(strip_prefix "${raw_folders[$i]}")" != '.stfolder' ]]; then
            folders+=("${raw_folders[$i]}")
        fi
        let i++
    done

    IFS=$'\n' folders=($(sort <<<"${folders[*]}"))
    unset IFS

    flen=${#folders[*]}
}

set_episodes() {
    unset episodes
    local root="${1}"

    while IFS=  read -r -d $'\0'; do
        episodes+=("$REPLY")
    done < <(find "$root" -maxdepth 1 -type f -print0)

    IFS=$'\n' episodes=($(sort <<<"${episodes[*]}"))
    unset IFS

    elen=${#episodes[*]}
}

print_menu() {
    i=0
    while [ $i -lt $flen ]; do
        echo "$((i+1))) $(strip_prefix "${folders[$i]}")"
        let i++
    done
    echo "$((i+1))) Quit"
}

prompt_podcast() {
    echo "Enter the number for the podcast to listen to (1-${flen}) or $(($flen + 1)) to quit"
    echo -n ">> "
}

read_podcast() {
    read index
    if [ $index -gt 0 ] && [ $index -le $flen ]; then
        echo "${folders[(($index - 1))]}"
        return 0
    elif [ $index -eq $(($flen + 1)) ]; then
        return 2
    else
        return 1
    fi
}

print_episodes() {
    i=0
    while [ $i -lt $elen ]; do
        echo "$((i+1))) $(strip_prefix "${episodes[$i]}" "$1")"
        let i++
    done
    echo "$((i+1))) Back"
}

prompt_episode() {
    echo "Enter the number for the episode to listen to (1-${elen}) or $(($elen + 1)) to quit"
    echo -n ">> "
}

read_episode() {
    read index
    if [ $index -gt 0 ] && [ $index -le $elen ]; then
        echo "${episodes[(($index - 1))]}"
        return 0
    elif [ $index -eq $(($elen + 1)) ]; then
        return 2
    else
        return 1
    fi
}

play_episode() {
    if ! player_exists $PLAYER; then
        echo "Please install $PLAYER and try again."
        return 1
    fi

    $PLAYER "$1"
}

prompt_delete() {
    if [ -f "$1" ]; then
        echo "1) Delete the file"
        echo "2) Keep the file"
        read -p ">> " del
        if [ $del -eq 1 ]; then
            rm "$1"
        fi
    fi
}

while true; do
    set_folders
    print_menu
    prompt_podcast
    pc="$(read_podcast)"
    ret=$?
    if [ $ret -ne 0 ] && [ $ret -ne 2 ]; then
        echo "An error occurred while getting the podcast to listen to. Press enter to continue."
        read unused
        continue
    elif [ $ret -eq 2 ]; then
        exit 0
    fi

    while true; do
        clear
        set_episodes "$pc"
        print_episodes "$pc"
        prompt_episode
        ep="$(read_episode)"
        if [ $? -eq 2 ]; then
            break
        fi
        play_episode "$ep"
        if [ $ret -ne 0 ]; then
            echo "An error occurred while playing the podcast episode. Press enter to continue."
            read unused
            continue
        fi
        prompt_delete "$ep"
    done
done
