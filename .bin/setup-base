#!/bin/sh

py="py37"
base_packages="anacron bash beets drm-kmod dsbmd dsbdriverd fish ffmpeg git gnupg keychain neovim newsboat p7zip perl5 powerdxx ${py}-borgbackup ${py}-dbus ${py}-powerline-status ${py}-psutil rcm rsync smartmontools sudo tmux"
cli_packages="cmus dsbmc-cli pianobar weechat youtube_dl"
game_packages="angband arx-libertatis bsdgames corsixth el gogrepo hedgewars higan lugaru manaplus mari0 mednafen minetest minetest_game mupen64plus openmw openrct2 stonesoup supertuxkart wesnoth xonotic"
kde_packages="ark choqok dolphin gwenview kaddressbook kate kcalc kdeconnect-kde kdialog kget kmail kmix konsole kontact korganizer krita okular phonon-qt5 plasma5-ksshaskpass plasma5-kwallet-pam plasma5-plasma plasma5-plasma-browser-integration print-manager sddm spectacle yakuake"
media_packages="musicpd musicpc kodi mdnsresponder"

anacron_rc="/etc/rc.conf.d/anacron"
cron_rc="/etc/rc.conf.d/cron"
devd_conf="/etc/devd.conf"
devd_rc="/etc/rc.conf.d/devd"
devfs_rules="/etc/devfs.rules"
drm_kmod_rc="/etc/rc.conf.d/kld"
dsbdriverd_rc="/etc/rc.conf.d/dsbdriverd"
dsbmd_rc="/etc/rc.conf.d/dsmbd"
fstab="/etc/fstab"
ntpd_rc="/etc/rc.conf.d/ntpd"
pcscd_rc="/etc/rc.conf.d/pcscd"
powerdxx_rc="/etc/rc.conf.d/powerdxx"
power_profile_rc="/etc/rc.conf.d/power_profile"
sendmail_rc="/etc/rc.conf.d/sendmail"
sudoers="/usr/local/etc/sudoers"
smartd_sample_conf="/usr/local/etc/smartd.conf.sample"
smartd_conf="/usr/local/etc/smartd.conf"
smartd_rc="/etc/rc.conf.d/smartd"

heredoc_var() {
    newline="
"
    result=""
    while IFS="${newline}" read -r line; do
        result="${result}${line}${newline}"
    done
    eval $1'="${result}"'
}

heredoc_var devfs_lines <<EOF
[devfsrules_common=7]
add path 'ad[0-9]\*'		mode 666
add path 'ada[0-9]\*'	mode 666
add path 'da[0-9]\*'		mode 666
add path 'acd[0-9]\*'	mode 666
add path 'cd[0-9]\*'		mode 666
add path 'mmcsd[0-9]\*'	mode 666
add path 'pass[0-9]\*'	mode 666
add path 'xpt[0-9]\*'	mode 666
add path 'ugen[0-9]\*'	mode 666
add path 'usbctl'		mode 666
add path 'usb/\*'		mode 666
add path 'lpt[0-9]\*'	mode 666
add path 'ulpt[0-9]\*'	mode 666
add path 'unlpt[0-9]\*'	mode 666
add path 'fd[0-9]\*'		mode 666
add path 'uscan[0-9]\*'	mode 666
add path 'video[0-9]\*'	mode 666
add path 'tuner[0-9]*'  mode 666
add path 'dvb/\*'		mode 666
add path 'cx88*' mode 0660
add path 'cx23885*' mode 0660 # CX23885-family stream configuration device
add path 'iicdev*' mode 0660
add path 'uvisor[0-9]*' mode 0660
EOF

heredoc_var fstab_lines <<EOF
proc        /proc                   procfs      rw                  0	0
fdesc       /dev/fd                 fdescfs     rw,late             0	0
EOF

heredoc_var fstab_linux_lines <<EOF
linprocfs   /compat/linux/proc      linprocfs   rw,late             0   0
linsysfs    /compat/linux/sys       linsysfs    rw,late             0   0
tmpfs       /compat/linux/dev/shm   tmpfs       rw,late,mode=1777   0   0
EOF

heredoc_var pcscd_lines <<EOF
attach 100 {
        device-name "ugen[0-9]+";
        action "/usr/local/sbin/pcscd -H";
};

detach 100 {
        device-name "ugen[0-9]+";
        action "/usr/local/sbin/pcscd -H";
};
EOF

heredoc_var sysctl_lines <<EOF
security.bsd.see_other_uids=0
security.bsd.see_other_gids=0
security.bsd.unprivileged_read_msgbuf=0
kern.randompid=1
security.bsd.stack_guard_page=1
vfs.zfs.min_auto_ashift=12
kern.ipc.shmmax=67108864
kern.ipc.shmall=32768
kern.sched.preempt_thresh=224
kern.maxfiles=200000
hw.syscons.bell=0
kern.ipc.shm_allow_removed=1
vfs.usermount=1
hw.acpi.lid_switch_state=S3
#kern.evdev.rcpt_mask=12
kern.evdev.rcpt_mask=6
hw.psm.synaptics.min_pressure=16
hw.psm.synaptics.max_pressure=220
hw.psm.synaptics.max_width=7
net.local.stream.recvspace=65536
net.local.stream.sendspace=65536
kern.coredump=0
hw.acpi.video.lcd0.economy=30
hw.acpi.video.lcd0.fullpower=60
EOF

heredoc_var loader_lines <<EOF
kern.vt=vt
kern.ipc.shmseg=1024
kern.ipc.shmmni=1024
kern.maxproc=100000
mmc_load="YES"
mmcsd_load="YES"
sdhci_load="YES"
atapicam_load="YES"
fuse_load="YES"
coretemp_load="YES"
snd_hda_load="YES"
tmpfs_load="YES"
aio_load="YES"
libiconv_load="YES"
libmchain_load="YES"
cd9660_iconv_load="YES"
msdosfs_iconv_load="YES"
boot_mute="YES"
autoboot_delay=2
aesni_load="YES"
cpuctl_load="YES"
acpi_hp_load="YES"
linux_load="YES"
linux64_load="YES"
cuse_load="YES"
#hw.psm.synaptics_support=1
hw.pci.do_power_nodriver=3
kern.hz=100
drm.i915.enable_rc6=7
drm.i915.semaphores="1"
drm.i915.intel_iommu_enabled="1"
legal.realtek.license_ack=1
$(sudo dmesg | grep -iq synaptics && echo "hw.psm.synaptics_support=1")
EOF

heredoc_var ntpd_lines <<EOF
ntpd_enable="YES"
ntpd_flags="-g"
EOF

heredoc_var power_profile_lines <<EOF
performance_cx_lowest="C2"
economy_cx_lowest="C2"
EOF

sendmail_lines='sendmail_enable="NONE"'

as_root() {
    if which sudo > /dev/null; then
        sudo $@
    else
        su root -c "$@"
    fi
}

comment_line() {
    file="$1"
    shift 1;
    pattern="$@"
    echo "$pattern"
    echo -e "s|^\\s${pattern}\\s\$|# ${pattern}|g"
    as_root sed -i '' -e "s|^\\s${pattern}\\s\$|# ${pattern}|g" "$file"
}

uncomment_line() {
    file="$1"
    shift 1;
    as_root sed -i '' -e "s|^#\\s*${pattern}\\s\$|${pattern}|g" "$file"
}

add_or_change_line() {
    file="$1"
    shift 1;

    line="$@"
    var="$(echo "$line" | cut -d '=' -f 1)"
    val="$(echo "$line" | cut -d '=' -f 2-)"

    if grep -q "^\s*${var}=" "$file"; then
        # replace
        sudo sed -i '' -e "s|^\s*${var}=.*|${line}|g" "$file"
    else
        # append
        echo "$line" | sudo tee -a "$file" > /dev/null
    fi
}

delete_text() {
    file="$1"
    text="$@"

    sudo perl -0pi -e "s|$text||g" "$file"
}

install_pkg() {
    as_root pkg install $base_packages
}

uninstall_pkg() {
    as_root pkg remove $base_packages
}

anacron_install() {
    echo 'anacron_enable="YES"' | sudo tee $anacron_rc
    echo 'cron_enable="NO"' | sudo tee $cron_rc

    comment_line /etc/crontab "1       3       *       *       *       root    periodic daily"
    comment_line /etc/crontab "15      4       *       *       6       root    periodic weekly"
    comment_line /etc/crontab "30      5       1       *       *       root    periodic monthly"
}

anacron_uninstall() {
    sudo rm $anacron_rc
    sudo rm $cron
    uncomment_line /etc/crontab "1       3       *       *       *       root    periodic daily"
    uncomment_line /etc/crontab "15      4       *       *       6       root    periodic weekly"
    uncomment_line /etc/crontab "30      5       1       *       *       root    periodic monthly"
}

devd_install() {
    echo 'devd_enable="YES"' | sudo tee "$devd_rc" > /dev/null
}

devd_uninstall() {
    sudo rm "$devd_rc"
}

devfs_install() {
    echo "$devfs_lines" | sudo tee "$devfs_rules" > /dev/null
}

devfs_uninstall() {
    delete_text "$devfs_rules" "$devfs_lines"
}

drm_kmod_install() {
    echo 'kld_list="/boot/modules/i915kms.ko acpi_video"' | sudo tee $drm_kmod_rc
}

drm_kmod_uninstall() {
    sudo rm $drm_kmod_rc
}

dsbdriverd_install() {
    echo 'dsbdriverd_enable="YES"' | sudo tee "$dsbdriverd_rc"
}

dsbdriverd_uninstall() {
    sudo rm "$dsbdriverd_rc"
}

dsbmd_install() {
    echo 'dsbmd_enable="YES"' | sudo tee "$dsbmd_rc"
}

dsbmd_uninstall() {
    sudo rm "$dsbmd_rc"
}

fstab_install() {
    echo "$fstab_lines" | while read -r line; do
        add_or_change_line "$fstab" "$line"
    done
}

fstab_uninstall() {
    echo "$fstab_lines" | while read -r line; do
        delete_text "$fstab" "$line"
    done
}

loader_install() {
    echo "$loader_lines" | while read -r line; do
        add_or_change_line /boot/loader.conf "$line"
    done
}

loader_uninstall() {
    echo "$loader_lines" | while read -r line; do
        delete_text /boot/loader.conf "$line"
    done
}

ntpd_install() {
    echo "$ntpd_lines" | sudo tee "$ntpd_rc" > /dev/null
    echo "$ntpd_lines" | while read -r line; do
        delete_text /etc/rc.conf "$line"
    done
}

ntpd_uninstall() {
    sudo rm "$ntpd_rc"
}

pcscd_install() {
    echo 'pcscd_enable="YES"' | sudo tee "$pcscd_rc" > /dev/null
    echo "$pcscd_lines" | sudo tee -a "$devd_conf" > /dev/null
}

pcscd_uninstall() {
    sudo rm $pcscd_rc
    if [ -f "$devd_conf" ]; then
        delete_text "$devd_conf" "$pcscd_lines"
    fi
}

powerdxx_install() {
    sudo tee "$powerdxx_rc" > /dev/null <<EOF
powerdxx_enable="YES"
powerdxx_flags="-a hiadaptive -b adaptive"
EOF
}

powerdxx_uninstall() {
    sudo rm $powerdxx_rc
}

power_profile_install() {
    echo "$power_profile_lines" | sudo tee "$power_profile_rc" > /dev/null
}

power_profile_uninstall() {
    sudo rm "$power_profile_rc"
}

sendmail_install() {
    echo "$sendmail_lines" | sudo tee "$sendmail_rc" > /dev/null
    echo "$sendmail_lines" | while read -r line; do
        delete_text /etc/rc.conf "$line"
    done
}

sendmail_uninstall() {
    sudo rm "$sendmail_rc"
}

smartd_install() {
    sudo cp "$smartd_sample_conf" "$smartd_conf"
    echo 'smartd_enable="YES"' | sudo tee "$smartd_rc" > /dev/null
}

smartd_uninstall() {
    sudo rm "$smartd_conf" "$smartd_rc"
}

sudo_install() {
    read -p "Take a moment to set up sudo privileges" unused
}

sysctl_install() {
    echo "$sysctl_lines" | while read -r line; do
        add_or_change_line /etc/sysctl.conf "$line"
        sudo sysctl "$line"
    done
}

sysctl_uninstall() {
    echo "$sysctl_lines" | while read -r line; do
        delete_text /etc/sysctl.conf "$line"
    done
}

case $1 in
    install|add)
        install_pkg
        sudo_install
        anacron_install
        devd_install
        devfs_install
        drm_kmod_install
        dsbdriverd_install
        fstab_install
        loader_install
        pcscd_install
        powerdxx_install
        smartd_install
        sysctl_install
        ;;
    uninstall|remove)
        anacron_uninstall
        devd_uninstall
        devfs_uninstall
        drm_kmod_uninstall
        dsbdriverd_uninstall
        fstab_uninstall
        loader_uninstall
        pcscd_uninstall
        powerdxx_uninstall
        smartd_uninstall
        sysctl_uninstall
        uninstall_pkg
        ;;
    *)
        echo "No such action for base: $1" 1>&2
        exit 1
        ;;
esac
